#include "game.h"

game::game(car my_car)
    : _my_car(my_car)
{
    /* CONSTRUCTOR */
}

bool game::is_my_car(std::string car_id)
{
    return this->_my_car.get_car_id() == car_id;
}

void game::add_car(std::string car_id, car& new_car)
{
    auto it = this->_cars.find(car_id);
    if (it != this->_cars.end()) /* car already added */
    {
        return;
    }

    this->_cars[car_id] = car(new_car);

    return;
}

void game::update_car(std::string car_id, const jsoncons::json& json_car)
{
    auto it = this->_cars.find(car_id);
    if (it == this->_cars.end()) /* didn't find car */
    {
        return;
    }

    car& to_update = this->_cars[car_id];

    to_update.set_angle(json_car["angle"].as<double>());

    jsoncons::json piecePosition = json_car["piecePosition"];
    to_update.set_piece_index(piecePosition["pieceIndex"].as<int>());
    to_update.set_in_piece_distance(piecePosition["inPieceDistance"].as<double>());
    to_update.set_start_lane(piecePosition["lane"]["startLaneIndex"].as<int>());
    to_update.set_end_lane(piecePosition["lane"]["endLaneIndex"].as<int>());
    to_update.set_lap(piecePosition["lap"].as<int>());

    return ;
}

car* game::get_car(std::string car_id)
{
    auto it = this->_cars.find(car_id);
    if (it == this->_cars.end())
    {
        return NULL;
    }

    return &this->_cars[car_id];
}

track* game::get_track()
{
    return this->_track;
}

void game::set_track(const jsoncons::json& json_track)
{
    this->_track = new track(json_track);
}

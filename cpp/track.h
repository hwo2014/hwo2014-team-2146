#ifndef HWO_TRACK_H
#define HWO_TRACK_H

#include <iostream>
#include <jsoncons/json.hpp>
#include "piece.h"
#include "lane.h"

class track
{
public:
    track(const jsoncons::json& json_track)
    {
        this->_id = json_track["id"].as<std::string>();
        this->_name = json_track["name"].as<std::string>();

        for (auto i = 0; i < json_track["pieces"].size(); i ++)
        {
            this->_pieces.push_back(piece(json_track["pieces"][i]));
        }

        for (auto i = 0; i < json_track["lanes"].size(); i ++)
        {
            this->_lanes.push_back(lane(json_track["lanes"][i]));
        }
    }

    piece get_piece(int index)
    {
        return this->_pieces[index];
    }

private:
    std::string _id;
    std::string _name;
    std::vector<piece> _pieces;
    std::vector<lane> _lanes;
};

#endif /* HWO_TRACK_H */

#ifndef HWO_LANE_H
#define HWO_LANE_H

#include <iostream>
#include <jsoncons/json.hpp>

class lane
{
public:
    lane(const jsoncons::json& json_lane)
    {
        this->_index = json_lane["index"].as<int>();
        this->_distance_from_center = json_lane["distanceFromCenter"].as<int>();
    }
private:
    int _index;
    int _distance_from_center;
};

#endif /* HWO_LANE_H */

#ifndef HWO_CAR_H
#define HWO_CAR_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

class car
{
public:
    car(std::string name, std::string color);
    car(const jsoncons::json&);
    car();

    std::string get_car_id()
    {
        return this->_name + " " + this->_color;
    }

    double get_angle()
    {
        return this->_angle;
    }
    void set_angle(double angle)
    {
        this->_angle = angle;
    }

    int get_piece_index()
    {
        return this->_piece_index;
    }
    void set_piece_index(int piece_index)
    {
        this->_piece_index = piece_index;
    }

    int get_in_piece_distance()
    {
        return this->_in_piece_distance;
    }
    void set_in_piece_distance(double in_piece_distance)
    {
        this->_in_piece_distance = in_piece_distance;
    }

    int get_start_lane()
    {
        return this->_start_lane;
    }
    void set_start_lane(int start_lane)
    {
        this->_start_lane = start_lane;
    }

    int get_end_lane()
    {
        return this->_end_lane;
    }
    void set_end_lane(int end_lane)
    {
        this->_end_lane = end_lane;
    }

    int get_lap()
    {
        return this->_lap;
    }
    void set_lap(int lap)
    {
        this->_lap = lap;
    }

private:
    std::string _color;
    std::string _name;

    double _length;
    double _width;
    double _guide_flag_position;

    double _angle;
    int _piece_index;
    double _in_piece_distance;
    int _start_lane;
    int _end_lane;
    int _lap;


};

#endif /* HWO_CAR_H */

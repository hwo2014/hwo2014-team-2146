#ifndef HWO_GAME_H
#define HWO_GAME_H

#include <map>
#include <iostream>
#include <jsoncons/json.hpp>
#include "car.h"
#include "track.h"

class game
{
public:
    game(car my_car);
    bool is_my_car(std::string car_id);
    void add_car(std::string car_id, car& new_car);
    void update_car(std::string car_id, const jsoncons::json& json_car);
    car* get_car(std::string car_id);
    track* get_track();
    void set_track(const jsoncons::json& json_track);
    void set_laps(int laps)
    {
        this->_laps = laps;
    }
    void set_max_lap_time(int max_lap_time)
    {
        this->_max_lap_time = max_lap_time;
    }
    void set_quick_race(bool quick_race)
    {
        this->_quick_race = quick_race;
    }
    void set_duration(int duration)
    {
        this->_duration = duration;
    }

private:
    car _my_car;
    track *_track;
    std::map<std::string, car> _cars;

    int _laps;
    int _max_lap_time;
    bool _quick_race;
    int _duration;
};

#endif /* HWO_GAME_H */

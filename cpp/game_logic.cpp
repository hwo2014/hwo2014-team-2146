#include <cmath>
#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
    : action_map({
        { "join", &game_logic::on_join },
        { "gameStart", &game_logic::on_game_start },
        { "carPositions", &game_logic::on_car_positions },
        { "crash", &game_logic::on_crash },
        { "gameEnd", &game_logic::on_game_end },
        { "error", &game_logic::on_error },
        { "yourCar", &game_logic::on_your_car },
        { "gameInit", &game_logic::on_game_init },
        { "turboAvailable", &game_logic::on_turbo_available }
    }), _game(NULL)
{
    /* constructor */
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& data = msg["data"];

    //std::cout << "Received a new message:" << msg << std::endl;

    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
        return (action_it->second)(this, data);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_ping() };
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
    std::cout << "Joined" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
    std::cout << "Race started" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    double throttle = 0.8;

    for (auto i = 0; i < data.size(); i ++)
    {
        std::string car_id = data[i]["id"]["name"].as<std::string>() + " " +
            data[i]["id"]["color"].as<std::string>();
        this->_game->update_car(car_id, data[i]);
        if (this->_game->is_my_car(car_id))
        {
            int piece_index = this->_game->get_car(car_id)->get_piece_index();

            if (abs(this->_game->get_track()->get_piece(piece_index).get_angle()) > 22.5)
                throttle -= 0.2;
            else if (abs(this->_game->get_track()->get_piece(piece_index).get_angle()) > 10)
                throttle -= 0.15;
            else if (abs(this->_game->get_track()->get_piece(piece_index + 1).get_angle()) > 10)
                throttle -= 0.1;
        }
    }

    return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
    std::cout << "Someone crashed" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
    std::cout << "Race ended" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
    std::cout << "Error: " << data.to_string() << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
    std::cout << "My car received" << std::endl;
    this->_game = new game(car(data["name"].as<std::string>(),
                data["color"].as<std::string>()));
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    std::cout << "Game init received" << std::endl;

    std::cout << "Message was " << data << std::endl;

    jsoncons::json cars = data["race"]["cars"];

    this->_game->set_track(data["race"]["track"]);

    for (auto i = 0; i < cars.size(); i ++)
    {
        car new_car(cars[i]);

        this->_game->add_car(new_car.get_car_id(), new_car);
    }

    try
    {
        this->_game->set_laps(data["race"]["raceSession"]["laps"].as<int>());
    }
    catch (jsoncons::json_exception& e)
    {
        /* no laps attribute */
    }

    try
    {
        this->_game->set_max_lap_time(data["race"]["raceSession"]["maxLapTimeMs"].as<int>());
    }
    catch (jsoncons::json_exception& e)
    {
        /* no maxLapTimeMs attribute */
    }

    try
    {
        this->_game->set_quick_race(data["race"]["raceSession"]["quickRace"].as<bool>());
    }
    catch (jsoncons::json_exception& e)
    {
        /* no quickRace attribute */
    }

    try
    {
        this->_game->set_duration(data["race"]["raceSession"]["durationMs"].as<int>());
    }
    catch (jsoncons::json_exception& e)
    {
        /* no durationMs attribute */
    }

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
    std::cout << "Received turbo available message : " << data << std::endl;
    return { make_ping() };
}

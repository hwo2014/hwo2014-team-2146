#include "car.h"

car::car(std::string name, std::string color)
    : _color(color), _name(name)
{
}

car::car()
{
}


car::car(const jsoncons::json& json_car)
{
    this->_name = json_car["id"]["name"].as<std::string>();
    this->_color = json_car["id"]["color"].as<std::string>();

    this->_length = json_car["dimensions"]["length"].as<double>();
    this->_width = json_car["dimensions"]["width"].as<double>();
    this->_guide_flag_position = json_car["dimensions"]["width"].as<double>();
}

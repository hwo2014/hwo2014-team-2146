#ifndef HWO_PIECE_H
#define HWO_PIECE_H

#include <jsoncons/json.hpp>

class piece
{
public:
    piece(const jsoncons::json& json_piece)
    {
        try
        {
            this->_length = json_piece["length"].as<double>();
        }
        catch (jsoncons::json_exception& e)
        {
            /* not a straight line */
        }

        try
        {
            this->_radius = json_piece["radius"].as<double>();
            this->_angle = json_piece["angle"].as<double>();
        }
        catch (jsoncons::json_exception& e)
        {
            /* not a curve */
        }

        try
        {
            this->_switch = json_piece["switch"].as<bool>();
        }
        catch (jsoncons::json_exception& e)
        {
            /* not a switch */
        }
    }

    double get_angle()
    {
        return this->_angle;
    }
private:
    double _length;
    bool _switch;
    double _radius;
    double _angle;
};

#endif /* HWO_PIECE_H */
